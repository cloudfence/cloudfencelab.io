# Website protection with OPNsense

With nginx plugin OPNsense become a strong full featured Web Application Firewall (WAF)

The OPNsense security platform can help you to protect your network and your webservers with the nginx plugin addition.

## Plugin installation

To install the plugin, follow this simple steps:

1. Access your OPNsense firewall web gui: (https://<YOUR_IP>)
2. Go to the Menu: System: Firmware: Plugins
3. Find the os-nginx plugin and click the install option [+]

![nginx install](../../images/nginx-1.gif)

## Initial configuration

Before the configuration, we need to understand some terms:

- Upstream Server: The real webserver that will host the web page/application;
- Upstream: The backend where will be configured the server(s);
- Location: The URL pattern that should have an Upstream configured;
- HTTP Server: The frontend that should have one or more Locations configured;

So, our configuration will start with the Upstream Server configuration:
To make the initial configuration, go to the Nginx configuration menu:

![nginx menu](../../images/nginx-2.png)

### Upstream Server

![nginx upstream](../../images/nginx-3.png)

![nginx edit upstream](../../images/nginx-4.png)

The server params will depend on your webserver/application and network environment. To better protection, it's recommended that you set some limits in Maximum Connections/Failures and in the Fail Timeout.

### Upstream

